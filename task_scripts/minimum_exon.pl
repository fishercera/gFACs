#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###########################################################################################################################################################
# ONLY EXONS ABOVE THIS SIZE MAY RIDE THE RIDE
#	About this script:
#		Sometimes you get very small exons, so small that they may not be real. So small that maybe the annotation was not clear about exactly
#	where the exon is and how big it is. Either way, we can remove whole genes that have one exon below the threshold. This can be given in the gFACs
#	argument.
###########################################################################################################################################################
# Arguments:
##ARGV[0] --> gene_table
##ARGV[1] --> Location
##ARGV[2] --> Minimum size
##ARGV[3] --> Pre
###########################################################################################################################################################
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";							# Get gene table + info
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
###########################################################################################################################################################
$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "minumum_exons_gene_table.txt";						# Name the output
open (OUTFILE, ">$output");												# Create the output
print OUTFILE "###", "\n";											

GENE:foreach $gene (@gene_table){
	if ($gene =~ /gene/){ $number_of_genes++;}
	$count = 0;
	$gene =~ s/^\s+\n//g;
	$gene =~ s/\s+$//;
	@split_lines = split "\n", $gene;	
	foreach $line (@split_lines){
		$line =~ s/\s+$//;
		@split_tab = split "\t", $line;
		if ($split_tab[0] =~ /exon/){			
			if ($split_tab[1] < $ARGV[2]){							# Real easy. Are you less thatn ARGV[2]?
				$count++;								# Then count
			}
		}
	}		
	if ($count == 0){										# If > 0, one exon was less than ARGV[2]		
		if ($gene =~ /gene/){
			print OUTFILE $gene, "\n", "###";						# Print those genes that pass
			$pass++;
		}
	}
}
###########################################################################################################################################################
$lost = $number_of_genes - $pass;									# What we all learned
print "Number of genes retained:\t", $pass, "\n";
print "Number of genes lost:\t", $lost, "\n";
close OUTFILE;
###########################################################################################################################################################
###########################################################################################################################################################
# Mint is a very aggressive plant and can be invasive. 
