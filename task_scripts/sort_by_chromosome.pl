#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
# SORT BY CHROMOSOME
#	About this script:
#		As a potential speed up for calling via bioperl. 
#################################################################################################################################################################

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";						# Name the gene table

open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";			# Get that gene table info
        @gene_table = <GENE_TABLE>;
        $gene_table = join "", @gene_table;
        @gene_table = split "###", $gene_table;
close GENE_TABLE;

$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "sorted_gene_table.txt";						# Name the output
open (OUTFILE, ">$output");											# Create the output
############################################################################################################################################################
foreach $gene (@gene_table){											# Parse that gene table
	if ($gene =~ /gene/){
                $gene =~ s/^\s+\n//g;
                $gene =~ s/\s+$//;
                @split_lines = split "\n", $gene;
                foreach $line (@split_lines){
                        $line =~ s/\s+$//;
                        @tab = split "\t", $line;
			$chromosomes{$tab[6]}++;								# Gather chromosomes
		}
	}
}
foreach $chromosome (keys %chromosomes){
	foreach $gene (@gene_table){                                                                                    # Parse that gene table
	        if ($gene =~ /gene/){
			$match = 0;
	                $gene =~ s/^\s+\n//g;
        	        $gene =~ s/\s+$//;
                	@split_lines = split "\n", $gene;
		                foreach $line (@split_lines){
                		        $line =~ s/\s+$//;
		                        @tab = split "\t", $line;
					if($tab[6] =~ $chromosome and $chromosome =~ $tab[6]){ $match = 1;};
				}
			if ($match == 1){ 
				print OUTFILE "###", "\n", $gene, "\n"; 
			}
		}
	}
}
close OUTFILE;
	
############################################################################################################################################################
# Did you know we are currently living in an ice age?
