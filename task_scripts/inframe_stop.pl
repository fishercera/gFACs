#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###########################################################################################################################################################
# DETECTING IN-FRAME STOPS
#	About this script:
#		Here, we are trying to figure out if there are any in-frame stop codons. In frame does NOT count the terminating stop codon. These pesky
#	little artifacts show up from time to time and indicate a potential psuedo gene or a gene missing an inserted base or missannotation. This script
#	works similarily to faa creation, but only analyzes the protein code and does not print it!
###########################################################################################################################################################

# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
# ARGV[2] --> threshold	(default is 0)
# ARGV[3] --> prefix

use Bio::Index::Fasta;				

$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "inframe_stop_filtered_gene_table.txt";				# Name the output
open (OUTPUT, ">$output") or die "Cannot create the output script!!!";						# Create the output

open (TEMPY, ">tempy.txt");

$input = $ARGV[0] . ".idx";											# Find the index
$inx = Bio::Index::Fasta->new(-filename => $input);								# Load the index

###########################################################################################################################################################
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";						# Name the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open $gene_table";						# Get all the information
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
###########################################################################################################################################################
# TRUSTY CODON TABLE
%aacode = (	
  TTT => "F", TTC => "F", TTA => "L", TTG => "L",
  TCT => "S", TCC => "S", TCA => "S", TCG => "S",
  TAT => "Y", TAC => "Y", TAA => "\*", TAG => "\*",
  TGT => "C", TGC => "C", TGA => "\*", TGG => "W",
  CTT => "L", CTC => "L", CTA => "L", CTG => "L",
  CCT => "P", CCC => "P", CCA => "P", CCG => "P",
  CAT => "H", CAC => "H", CAA => "Q", CAG => "Q",
  CGT => "R", CGC => "R", CGA => "R", CGG => "R",
  ATT => "I", ATC => "I", ATA => "I", ATG => "M",
  ACT => "T", ACC => "T", ACA => "T", ACG => "T",
  AAT => "N", AAC => "N", AAA => "K", AAG => "K",
  AGT => "S", AGC => "S", AGA => "R", AGG => "R",
  GTT => "V", GTC => "V", GTA => "V", GTG => "V",
  GCT => "A", GCC => "A", GCA => "A", GCG => "A",
  GAT => "D", GAC => "D", GAA => "E", GAG => "E",
  GGT => "G", GGC => "G", GGA => "G", GGG => "G",
);
###########################################################################################################################################################
$old_seq = "new";
GENE: foreach $gene (@gene_table){										# Parse the gene table
if ($gene =~ /gene/){
        $gene =~ s/^\s+\n//g;
	$number_of_genes++;
        @split_lines = split "\n", $gene;	
        foreach $line (@split_lines){
                $line =~ s/\s+$//;
                @split_tab = split "\t", $line;
			if ($split_tab[0] =~ /gene/){								# Gene line
					if ($split_tab[6] ne $old_seq){
                                                $seq = $inx->fetch("$split_tab[6]");                            # Gets scaffold sequence
                                        }
                                        $old_seq = $split_tab[6];
			}
			if ($split_tab[0] =~ /exon/){								# Exon line
                                $start = $split_tab[2];
                                $end = $split_tab[3];
				
                                $string_1 = $seq->subseq("$start", "$end");					# Grab the CDS
				push @sequence, $string_1;							# Add it onto an array
			}
	}                         
      		$join_string = join "", @sequence;								# Make the array a string
		@string = split "", $join_string;
	if ($split_tab[4] =~ /\+/){										# Split nt by nt
		for ($a = 0; $a < scalar @string; $a = $a +3){								# Codon by codon
			$codon = $string[$a] . $string[$a+1] . $string[$a+2];
                        $codon =~ tr/atgc/ATGC/;

                      	if ($aacode{$codon} =~ /\*/){ $instop++;}						# If "*", count
			push @aasequence, $aacode{$codon};							# Add forward onto array
                }
	}
	if ($split_tab[4] =~ /\-/){
                for ($a = scalar @string - 1; $a >= 0; $a = $a - 3){                                     # If negative, we are switching and inverting the se$
                        $codon = $string[$a] . $string[$a-1] . $string[$a - 2];
                        $codon =~ tr/atgc/ATGC/;
                        $codon =~ tr/ATGC/TACG/;
			if ($aacode{$codon} =~ /\*/){ $instop++;}
                        push @aasequence, $aacode{$codon};                                             # Then storing it in reverse for a forward M-->* pro$
                }
        }

	if ($aasequence[scalar @aasequence - 1] =~ /\*/){							# if last codon is a *, subtract 1 from count
		$instop = $instop - 1;
	}
        if ($instop <= $ARGV[2]){										# If you have less than inputted number
		print OUTPUT "###", "\n", $gene, "\n";								# Print
		$saved++;											# Count
	}
	if ($instop > $ARGV[2] ){                                                                               # If you have less than inputted number
                print TEMPY "###", "\n", $gene, "\n", @aasequence, "\n";                                                                # Print
        }
	$instop = 0;												# Resets
	undef (@aasequence);
	undef (@sequence);
}
}
close OUTPUT;
###########################################################################################################################################################
$lost = $number_of_genes - $saved;										# What we learned today
print "Number of genes retained:\t", $saved, "\n";
print "Number of genes lost:\t", $lost, "\n";
###########################################################################################################################################################
###########################################################################################################################################################
# Did you know in Denmark you get 5 weeks paid vacation a year? 

