#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
#ARGV[0] --> location
#ARGV[1] --> prefix
#################################################################################################################################################################
$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";
open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table!!!";
	@gene_table = <GENE_TABLE>;
	$join = join "", @gene_table;
	@gene_table = split "###", $join;
close GENE_TABLE;
#################################################################################################################################################################
$output = $ARGV[0] . "\/" . $ARGV[1] . "data_exon_position_distributions.tsv";
open (OUTPUT, ">$output") or die "Cannot create $output";
	foreach $gene (@gene_table){
		$gene =~ s/\s+$//;
		$gene =~ s/^\s+\n//g;
		@lines = split "\n", $gene;
		foreach $line (@lines){
			@tab = split "\t", $line;
			if ($tab[0] =~ /exon/){
				if ($tab[4] =~ /\+/){push @exons, $tab[1];}
				if ($tab[4] =~ /\-/){unshift @exons, $tab[1];}
			}
		}
		foreach $exon_size (@exons){
			$exon_count++;
			push @$exon_count, $exon_size;
			$exon_num_range{$exon_count} = "count";
			$exon_num_sizes{$exon_count} = $exon_num_sizes{$exon_count} +  $exon_size;
		}
		$exon_num_range_gene{scalar @exons}++;
		$exon_count = 0;
		undef (@exons);
	}
#################################################################################################################################################################
foreach $exon_size (sort {$a <=> $b} keys %exon_num_range){
		print OUTPUT $exon_size, "\t";
		foreach $value (@$exon_size){
			print OUTPUT $value, "\t";
		}
		print OUTPUT "\n";
}
close OUTPUT;
#################################################################################################################################################################
#################################################################################################################################################################
# The lowest credit score possible is 300 and that is very very very bad. 

