#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
# 

#ARGV[0] --> location
#ARGV[1] --> prefix

$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";
open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table!!!";
	@gene_table = <GENE_TABLE>;
	$join = join "", @gene_table;
	@gene_table = split "###", $join;
close GENE_TABLE;

$output = $ARGV[0] . "\/" . $ARGV[1] . "intron_position_distributions.tsv";
open (OUTPUT, ">$output") or die "Cannot create $output";
	foreach $gene (@gene_table){
		
		$gene =~ s/\s+$//;
		$gene =~ s/^\s+\n//g;

		@lines = split "\n", $gene;
		foreach $line (@lines){
			@tab = split "\t", $line;

			if ($tab[0] =~ /intron/){
				if ($tab[4] =~ /\+/){push @introns, $tab[1];}
				if ($tab[4] =~ /\-/){unshift @introns, $tab[1];}
			}
		}
		foreach $intron_size (@introns){
			$intron_count++;
			push @$intron_count, $intron_size;
			$intron_num_range{$intron_count} = "count";
			$intron_num_sizes{$intron_count} = $intron_num_sizes{$intron_count} +  $intron_size;
		}
		$intron_num_range_gene{scalar @introns}++;
		$intron_count = 0;
		undef (@introns);
	}

print OUTPUT "intron_position", "\t", 
	"n(Individual introns)", "\t",
	"n(Max introns in gene)", "\t", 
	"average_size", "\t", 
	"median_size", "\t", 
	"Min", "\t",
	"Max", "\t";
print OUTPUT "\n";

foreach $intron_size (sort {$a <=> $b} keys %intron_num_range){
	print OUTPUT $intron_size, "\t", 									# Positions starting at 1
		scalar @$intron_size, "\t";								# Num of introns supporting location
	
		if ( $intron_num_range_gene{$intron_size} =~ /./){
			print OUTPUT $intron_num_range_gene{$intron_size}, "\t",				# intron containment distribution
		}
		else{ print OUTPUT "0", "\t"}
	
		$average = $intron_num_sizes{$intron_size} / scalar @$intron_size;				# Average per position
		$average_rounded = sprintf("%.3f", $average);						# 3 decimals
	
		print OUTPUT $average_rounded, "\t";	

		@sorted_array = sort {$a <=> $b} @$intron_size;
		        $middle_of_sorted_array = scalar @sorted_array / 2;
		        if ($middle_of_sorted_array =~ /\./){
	                	$median_value_int = int($middle_of_sorted_array) ;
		                $median_value_red = int($middle_of_sorted_array)  +1;
	        	        $median_size = ($sorted_array[$median_value_int] + $sorted_array[$median_value_red]) /2;
	        	}
	        	elsif($middle_of_sorted_array =~ /\d+$/){
	                	$median_value = $middle_of_sorted_array;
		                $median_size = $sorted_array[$median_value];
        		}
	
		print OUTPUT $median_size, "\t";


		print OUTPUT $sorted_array[0], "\t", $sorted_array[scalar @sorted_array - 1], "\t";		# Min and Max sizes of intron at position
		print OUTPUT "\n";
}
close OUTPUT;

