#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
###########################################################################################################################################################
# ONLY INTRONS ABOVE THIS SIZE MAY CONTINUE
#       About this script:
#               Sometimes you get very small introns, so small that they may not be real. So small that maybe the annotation was not clear about exactly
#       where the intron is and how big it is. Either way, we can remove whole genes that have one intron below the threshold. This can be given in the gFACs
#       argument.
###########################################################################################################################################################
##ARGV[0] --> gene_table
##ARGV[1] --> Location
##ARGV[2] -->  Minimum size
##ARGV[3] --> Pre
###########################################################################################################################################################
$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";					# Snag the gene table
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";		# Take its information
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
###########################################################################################################################################################
$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "minumum_introns_gene_table.txt";				# Name output
open (OUTFILE, ">$output");										# Create output
print OUTFILE "###", "\n";
###########################################################################################################################################################
GENE:foreach $gene (@gene_table){
	if ($gene =~ /gene/){$number_of_genes++;}
	$count = 0;
	$gene =~ s/^\s+\n//g;
	$gene =~ s/\s+$//;
	@split_lines = split "\n", $gene;
	foreach $line (@split_lines){
		$line =~ s/\s+$//;
		@split_tab = split "\t", $line;
		if ($split_tab[0] =~ /intron/){								# Are we dealing with an intron?
			if ($split_tab[1] < $ARGV[2]){							# Is this intron smaller than the designated minimum? 
				$count++;		
			}
		}
	}
	if ($count == 0){										# All introns are above the designated minimum
		if ($gene =~ /gene/){
			print OUTFILE $gene, "\n", "###\n";						# Prin 'em
			$pass++;
		}
	}
}
###########################################################################################################################################################
$lost = $number_of_genes - $pass;									# Summary
print "Number of genes retained:\t", $pass, "\n";
print "Number of genes lost:\t", $lost, "\n";
close OUTFILE;
###########################################################################################################################################################
###########################################################################################################################################################
# Sea water freezes at 28.4 degrees Fahrenheit (-2C)
