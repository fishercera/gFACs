#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
######################################################################################################################################################
# WE HAVE TO HAVE TO HAVE A STOP OR A START CODON
#	About this script:
#		This script combines the abilities of having a start codon OR a stop codon. You must have at least one of these essential
#	codons to pass. It doesn't matter if it is the start or stop (or both -- that is fine too).
######################################################################################################################################################
# Arguments:
# ARGV[0] --> in fasta
# ARGV[1] --> output location
######################################################################################################################################################
$codon_present = 0;
use Bio::Index::Fasta;
######################################################################################################################################################
$output = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "start_and_stop_gene_table.txt";				# Name output
open (OUT_TABLE, ">$output") or die "Cannot create the output script!!!";					# Create output
print OUT_TABLE "###", "\n";

$input = $ARGV[0] . ".idx";											# Index
$inx = Bio::Index::Fasta->new(-filename => $input);

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[2] . "gene_table.txt";
open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";			# Gene table info
	@gene_table = <GENE_TABLE>;
	$gene_table = join "", @gene_table;
	@gene_table = split "###", $gene_table;
close GENE_TABLE;
######################################################################################################################################################
$old_seq = "new";
GENE: foreach $gene (@gene_table){
	$end_check = 0;
	$start_check = 0;
	if ($gene =~ /gene/){$number_of_genes++;}
		$frame = 0;                                                                                             # Paranoia number reset early
                $exon_size = 0;
                $gene =~ s/^\s+\n//g;
                @split_lines = split "\n", $gene;

                       foreach $line (@split_lines){                                                                    # Parsing the gene
                                $line =~ s/\s+$//;
                                @tab = split "\t", $line;
                                if ($tab[0] =~ /exon/){$exon_size = $exon_size + $tab[1];}                              # We're tallying the CDS
                        }
                        $frame = $exon_size / 3;                                                                        # Divide CDS by 3
                        if ($frame =~ /\./){next GENE;}                                                                 # Decimal. Clearly not divisible by 3
                        if ($frame !~ /\./){                                                                            # Non decimal = okay
				$end_check--;
                        }

	        foreach $line (@split_lines){
		$line =~ s/\s+$//;										# Print gene, exon, intron line
                $line =~ s/\s+$//;
                @split_tab = split "\t", $line;
                        if ($split_tab[0] =~ /gene/){
				if ($split_tab[4] =~ /\+/){							
					if ($split_tab[6] ne $old_seq){
                                                $seq = $inx->fetch("$split_tab[6]");                            # Gets scaffold sequence
                                        }
                                        $old_seq = $split_tab[6];

					$end = $split_tab[3] -2;						# In a pos strand, stop is last 3 positions.		
					$end_end = $split_tab[3];
					$length = $seq->length();						# Gets the length of the scaffold
					if ($length > $end_end){						
                                              $string_end_1 = $seq->subseq($end, $end_end);
                                              $string_end_1 =~ tr/ATGC/atgc/;
					      if ($string_end_1 =~ /taa/ or $string_end_1 =~ /tga/ or $string_end_1 =~ /tag/){
							if ($line !~ /3_INC/){
							$end_check = 1;
							$codon_present++;						# stop codon detected
							undef ($string_end_1);
							}
						}
                                        }
					$start = $split_tab[2];                                                         #1
                                        $start_end = $split_tab[2] +2;                                                  #3

                                        $string_1 = $seq->subseq($start, $start_end);                                   #seq = 1-3
                                        $string_1 =~ tr/ATGC/atgc/;                                                     #all lower case
                                        $length = $seq->length();
                                                if ($string_1 =~ /atg|gtg|ttg/ and $line !~ /5_INC/){
                                                        $start_check = 1;
                                                   	$codon_present++;
                                                        undef ($string_1);
                                                }
                                

				}
				if ($split_tab[4] =~ /\-/){
					$end = $split_tab[2];							# NEG strand
					$end_end = $split_tab[2] +2;
						if ($split_tab[6] ne $old_seq){
                	                                $seq = $inx->fetch("$split_tab[6]");                            # Gets scaffold sequence
 	                                        }
        	                                $old_seq = $split_tab[6];

						$length = $seq->length();
                                                if ($end >= 1){							# Rev strand stop is pre [2]	
							$string_end = $seq->subseq($end, $end_end);
							$string_end =~ tr/ATGC/atgc/;
                                		        $string_end =~ tr/atgc/tacg/;
							@split_neg_end = split "",  $string_end;
	                                        	@split_neg_end = split "",  $string_end;
							$string_2_end = $split_neg_end[2] . $split_neg_end[1] . $split_neg_end[0];
							if ($string_2_end =~ /taa/ or $string_2_end =~ /tga/ or $string_2_end =~ /tag/){
								if ($line !~ /3_INC/){
		                                                $end_check = 1;
								$codon_present++;
								undef($string_end_1);
								undef($string_end);
        	                	                }
						
						 	$start_end =  $split_tab[3];                                    #In reverse due to strandedness
		                                         $start = $split_tab[3] -2;
	
		                                        $string_1 = $seq->subseq($start, $start_end);
        		                                $string_1 =~ tr/ATGC/atgc/;
                		                        $string_1 =~ tr/atgc/tacg/;
                        		                @split_neg = split "",  $string_1;
                                		        $string_2 = $split_neg[2] . $split_neg[1] . $split_neg[0];              #inverting then rearranging
                                      		        if ($string_2 =~ /atg|gtg|ttg/ and $line !~ /5_INC/){
                                             		        $start_check = 1;
                                                       		$codon_present++;
	                                                        undef ($string_2);
        	                                                undef ($string_1);
                	                                }
						}
					}
				}
			}
		}
		if ($end_check == 1 or $start_check == 1){							# Was there a stop or start codon?
			print OUT_TABLE $gene, "\n", "###";
			$pass_pass++;
		}
		$start = 0;
		$end_check = 0;
	}
close OUT_TABLE;
######################################################################################################################################################
$lost = $number_of_genes - $pass_pass;
print "Number of genes retained:", "\t", $pass_pass, "\n";
print "Number of genes lost:", "\t", $lost, "\n";
######################################################################################################################################################
######################################################################################################################################################
#It’s illegal to own only one guinea pig in Switzerland. (They get lonely.)
