#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
# CDS SIZE CUT OFF
#	About this script:
# 		This script is designed to have a cut off for minimum CDS size in a gene. It translates to mimimum number of amino acids a gene must have. 
#	Genes have a limit for what is too small. Based on current research, the smallest gene is 74nt long and is a tRNA. A gene probably cannot be 3 nt long. 
#	That is ridiculous. However, if a smaller gene is discovered and you lose it, just set the number smaller! Default is only 74. 
#################################################################################################################################################################

$gene_table = "$ARGV[1]" . "\/" . "\/" . $ARGV[3] . "gene_table.txt";						# Name the gene table

##ARGV[1] --> Location
##ARGV[2] --> Minimum size
##ARGV[3] --> Pre

open (GENE_TABLE, "$gene_table") or die "Cannot open the gene_table.txt file at $ARGV[1]";			# Get that gene table info
        @gene_table = <GENE_TABLE>;
        $gene_table = join "", @gene_table;
        @gene_table = split "###", $gene_table;
close GENE_TABLE;

$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "minumum_CDS_gene_table.txt";					# Name the output
open (OUTFILE, ">$output");											# Create the output
	print OUTFILE "###", "\n";
#################################################################################################################################################################
foreach $gene (@gene_table){											# Parse that gene table
	if ($gene =~ /gene/){
		$number_of_genes++;										# Tally
		$gene =~ s/^\s+\n//g;
		$gene =~ s/\s+$//;
		@split_lines = split "\n", $gene;			
		foreach $line (@split_lines){
			$line =~ s/\s+$//;
			@tab = split "\t", $line;
			if ($tab[0] =~ /exon/){$exon_size = $exon_size + $tab[1];}				# If we have an exon, track its size.
		}
			if ($exon_size >= $ARGV[2]){								# If > than or = to inputted cutoff value
				$saved++;									# Tally
				print OUTFILE $gene, "\n", "###", "\n";						# And print 
			}
		}
	$exon_size = 0;												# Reset CDS size for each gene
	}

close OUTFILE;

$lost = $number_of_genes - $saved;										# Results print
print "Number of genes retained:\t", $saved, "\n";
print "Number of genes lost:\t", $lost, "\n";

#################################################################################################################################################################
#################################################################################################################################################################
# That wasn't so bad. Did you know there are no wild kangaroos in New Zealand. But there are wild (non-native) wallabies.
