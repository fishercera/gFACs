#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
#################################################################################################################################################################
# EDITING GENES TO REFLECT THEIR EXONS
#       About this script:
#		Many annotation pipelines create models that are incomplete. This script finds them and reassigns the gene length to reflect this.
#	The genes are then marked as incomplete in the 8th column of the gene table.
#################################################################################################################################################################

#ARGV[0] == output destination
#ARGV[1] == prefix

$gene_table = $ARGV[0] . "\/" . $ARGV[1] . "gene_table.txt";                                                    # Name the gene table
$output = $ARGV[0] . "\/" . $ARGV[1] . "new_gene_table.txt";	                                                # Name the output

open (GENE_TABLE, $gene_table) or die "Cannot open $gene_table!!!";                                             # Snag the information from the gene table
        @gene_table = <GENE_TABLE>;
        $join = join "", @gene_table;
        @gene_table = split "###", $join;
close GENE_TABLE;

open (OUTPUT, ">$output") or die "Cannot create $output!!!";                                                    # Create the output

#################################################################################################################################################################
print OUTFILE "###", "\n";
foreach $gene (@gene_table){                                                                                    # Parse
        if ($gene =~ /gene/){                                                                                   # Is this a gene?
                @split_lines = split "\n", $gene;
                foreach $line (@split_lines){
                        $line =~ s/\s+$//;
                        @tab = split "\t", $line;
                        if ($tab[0] =~ /gene/){     
				$exon_count = 0;
					 if ($tab[4] =~ /\+/){						
						$gene_start = $tab[2];
						$gene_end = $tab[3];
					 }
					 if ($tab[4] =~ /\-/){                                                  
                                                $gene_start = $tab[3];
                                                $gene_end = $tab[2];
                                        }

			}
			if ($tab[4] =~ /\+/){									# If this is a + gene	
				if($tab[0] =~ /exon/){								# If this is an exon line
					$exon_count++;
					if ($exon_count == 1){ $start = $tab[2]; }				# + strand start pos exon
					$end = $tab[3]; 							# Will become the ending 
				}
			}
			if ($tab[4] =~ /\-/){                                                                   # If this is a + gene
                                if($tab[0] =~ /exon/){                                                          # If this is an exon line
                                        $exon_count++;
                                        if ($exon_count == 1){ $end = $tab[2]; }                                # + strand start pos exon
                                        $start = $tab[3];    	                                                # Will become the ending
                                }
                        }
		}

		# The whole gene is parsed. Now to compare if things are awry.

		if($gene_start !~ $start and  $gene_end == $end){    				# 5' INC
			foreach $line (@split_lines){
                        $line =~ s/\s+$//;
              		        @tab = split "\t", $line;
	                        if ($tab[0] =~ /gene/  and $tab[4] =~ /\+/){
					print OUTPUT $tab[0], "\t",
					 $tab[3] - $start + 1, "\t",
					 $start, "\t",
					 $tab[3], "\t",
					 $tab[4], "\t",
					 $tab[5], ";5_INC", "\t", $tab[6], "\n";
				}
				 if ($tab[0] =~ /gene/  and $tab[4] =~ /\-/){
                                        print OUTPUT $tab[0], "\t",
                                         $start - $tab[2] + 1, "\t",
                                         $tab[2], "\t",
                                         $start, "\t",
                                         $tab[4], "\t",
                                         $tab[5], ";5_INC", "\t", $tab[6], "\n";
                                }
			}
		}
		if($gene_end !~ $end and $gene_start == $start){                                # 3' INC
                       foreach $line (@split_lines){
                       $line =~ s/\s+$//;
                               @tab = split "\t", $line;
                               if ($tab[0] =~ /gene/ and $tab[4] =~ /\+/){
                                        print OUTPUT $tab[0], "\t",
                                          $end - $tab[2] + 1, "\t",
                                          $tab[2], "\t",
                                          $end, "\t",
                                          $tab[4], "\t",
                                          $tab[5], ";3_INC", "\t", $tab[6], "\n";
                               }
				if ($tab[0] =~ /gene/ and $tab[4] =~ /\-/){
                                        print OUTPUT $tab[0], "\t",
                                          $tab[3] - $end + 1, "\t",
                                          $end, "\t",
                                          $tab[3], "\t",
                                          $tab[4], "\t",
                                          $tab[5], ";3_INC", "\t", $tab[6], "\n";
                               }
                       }
		}
		 if($gene_start == $start and $gene_end == $end){                                    # COMP
                        foreach $line (@split_lines){
                        $line =~ s/\s+$//;
                                @tab = split "\t", $line;
                                if ($tab[0] =~ /gene/){
                                        print OUTPUT $tab[0], "\t",
                                         $tab[1], "\t",
                                         $tab[2], "\t",
                                         $tab[3], "\t",
                                         $tab[4], "\t",
                                         $tab[5], ";COMP", "\t", $tab[6], "\n";
                                }
				
                        }
                }
		if($gene_start !~ $start and $gene_end !~ $end){                                    # COMP
                        foreach $line (@split_lines){
                        $line =~ s/\s+$//;
                                @tab = split "\t", $line;
                                if ($tab[0] =~ /gene/ and $tab[4] =~ /\+/){
                                        print OUTPUT $tab[0], "\t",
                                         $end - $start + 1, "\t",
                                         $start, "\t",
                                         $end, "\t",
                                         $tab[4], "\t",
                                         $tab[5], ";5_INC+3_INC", "\t", $tab[6], "\n";
                                }
				if ($tab[0] =~ /gene/ and $tab[4] =~ /\+/){
                                        print OUTPUT $tab[0], "\t",
                                         $end - $start + 1, "\t",
                                         $start, "\t",
                                         $end, "\t",
                                         $tab[4], "\t",
                                         $tab[5], ";5_INC+3_INC", "\t", $tab[6], "\n";
                                }
				if ($tab[0] =~ /gene/ and $tab[4] =~ /\-/){
                                        print OUTPUT $tab[0], "\t",
                                         $start - $end + 1, "\t",
                                         $end, "\t",
                                         $start, "\t",
                                         $tab[4], "\t",
                                         $tab[5], ";5_INC+3_INC", "\t", $tab[6], "\n";
                                }

                        }
                }
		foreach $line (@split_lines){
                        $line =~ s/\s+$//;
                               @tab = split "\t", $line;
                               if ($tab[0] !~ /gene/ and $tab[0] =~ /./){
                                        print OUTPUT $tab[0], "\t",
                                          $tab[1], "\t",
                                          $tab[2], "\t",
                                          $tab[3], "\t",
                                          $tab[4], "\t",
                                          $tab[5], "\t",
					  $tab[6], "\n";
                               }
                }		
		print OUTPUT "###", "\n";	
	}			
}

######################################################################################################################################################
# Only female polar bears hibernate
