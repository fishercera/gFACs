#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# STRINGTIE compatibility gtf
#	About this script:
#		Creates compatibility output for STRINGTIE (1.3.6) gtf
##################################################################################################################################################################
#$ARGV[0] = gene table
#$ARGV[1] = output location
#$ARGV[3] = prefix
#$ARGV[2] = input gene file.

open (INPUT, $ARGV[2]) or die "Cannot open $ARGV[2]";			# We are getting the ORIGINAL input.

while ($line = <INPUT>){						# Parse the input
	$line =~ s/\s+$//;
	undef (@array);
        undef (@sort);
	@split_line = split "\t", $line;
		push @array, $split_line[3];   				# start
		push @array, $split_line[4];   				# stop
		
	@sort = sort {$a <=> $b} @array;				# correcting for potential disorder
		$ID = $sort[0] . "_" . $sort[1];			# Making ID
	
	if ($Score_to_id{$ID} =~ /\d/){					# If a number is already stored
		if ($Score_to_id{$ID} < $split_line[5]){		# And if the number stored is smaller
			$Score_to_id{$ID} = $split_line[5];		# Replace it
		}
	}
	else{
		$Score_to_id{$ID} = $split_line[5];			# Otherwise store what you've got
	}
	undef (@array);
	undef (@sort);
}
close INPUT;
##################################################################################################################################################################
$output = $ARGV[1] . "\/" . "\/" . $ARGV[3] . "stringtie_1.3.6.gtf";	# Name the output

open (GTF, ">$output") or die "Cannot create $output";			# Creat the output

open (GENE_TABLE, $ARGV[0]) or die "Cannot open $ARGV[0]";		# Our friend the gene table
while ($line = <GENE_TABLE>){
	$line =~ s/\;COMP//;
        $line =~ s/\;5_INC\+3_INC//;
        $line =~ s/\;3_INC//;
        $line =~ s/\;5_INC//;

	$line =~ s/\s+$//;
	if ($line =~ /\#\#\#/){next;}					# We don't care about partition lines
	if ($line =~ /./){
		@split_line = split "\t", $line;
		$start_stop = $split_line[2] . "_" . $split_line[3];
		$score = ".";
		if ($Score_to_id{$start_stop} =~ /./){
			$score = $Score_to_id{$start_stop};		# Score reset
		}
		if ($split_line[0] =~ /gene/){
			$gene_count++;
			$split_line[5] =~ s/ID=//;
			$ID = $split_line[5];
		}

		if ($split_line[0] =~ /exon/){				
			$split_line[5] =~ s/Parent=//;
                        $tran_id = $split_line[5];

			print GTF $split_line[6], "\t",			# scaffold
				"GFACS", "\t",					# Source. Me!
				"exon", "\t",				# Gene part
				$split_line[2], "\t",				# Start
				$split_line[3], "\t",				# Stop
				$score, "\t",					# Score, if we have it. "." if not.
				$split_line[4], "\t",				# Strand
				".", "\t",					# Something I cannot provide
				"gene_id \"" . $ID . "\"; ",
				"transcript_id \"" . $tran_id . "\"; ",
				"gene_name \"" . $ID . "\"; \n";

			 print GTF $split_line[6], "\t",                        # scaffold
                                "GFACS", "\t",                                  # Source. Me!
                                "CDS", "\t",                          # Gene part
                                $split_line[2], "\t",                           # Start
                                $split_line[3], "\t",                           # Stop
                                $score, "\t",                                   # Score, if we have it. "." if not.
                                $split_line[4], "\t",                           # Strand
                                ".", "\t",                                      # Something I cannot provide
                                "gene_id \"" . $ID . "\"; ",
                                "transcript_id \"" . $tran_id . "\"; ",
                                "gene_name \"" . $ID . "\"; \n";
 
		}
	}
}		
close GENE_TABLE;
close GTF;
##################################################################################################################################################################
##################################################################################################################################################################
# As of 2020, the most powerful passport belongs to the UAE. 
