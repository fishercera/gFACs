#!usr/bin/perl
# This script is a part of gFACs: https://gitlab.com/PlantGenomicsLab/gFACs
# Copyright (C) 2018 Madison Caballero
##################################################################################################################################################################
# COMPATIBILITY: EVM GENE PREDICTION (EVM version 1.1.1)
#       About this script:
#               This script will take your gtf as an input and get it all ready and formatted for EVM.
##################################################################################################################################################################
#$ARGV[0] = output location
#$ARGV[1] = prefix

$gtf = $ARGV[0] . "\/" . $ARGV[1] . "out.gtf";
$output =  $ARGV[0] . "\/" . $ARGV[1] . "EVM_1.1.1_gene_prediction_format.gff3";

open (GTF, $gtf) or die "Cannot open $gtf!!!";
open (OUTPUT, ">$output") or die "Cannot create $output!!!";

while ($line = <GTF>){
	$line =~ s/\s+$//;
	$line =~ s/ID\=//;
	$line =~ s/\;COMP//;
        $line =~ s/\;5_INC\+3_INC//;
	$line =~ s/\;3_INC//;
	$line =~ s/\;5_INC//;
	@split = split "\t", $line;
	if ($split[2] =~ /gene/){
		print OUTPUT $split[0], "\t",
		$split[1], "\t",
		$split[2], "\t",
		$split[3], "\t",
		$split[4], "\t",
		$split[5], "\t",
		$split[6], "\t",
		$split[7], "\t",
		"ID=",$split[8], ";Name=", $split[8], "\n";

		print OUTPUT $split[0], "\t",
                $split[1], "\t",
                "mRNA", "\t",
                $split[3], "\t",
                $split[4], "\t",
                $split[5], "\t",
                $split[6], "\t",
                $split[7], "\t",
                "ID=",$split[8], ";Parent=", $split[8], "\n";
	
		$name = $split[8];
	}
	if ($split[2] =~ /CDS/){
                print OUTPUT $split[0], "\t",
                $split[1], "\t",
                "CDS", "\t",
                $split[3], "\t",
                $split[4], "\t",
                $split[5], "\t",
                $split[6], "\t",
                $split[7], "\t",
                "ID=",$split[8], ";Parent=", $name,
                "\n";

		print OUTPUT $split[0], "\t",
                $split[1], "\t",
                "exon", "\t",
                $split[3], "\t",
                $split[4], "\t",
                $split[5], "\t",
                $split[6], "\t",
                $split[7], "\t",
                "ID=cds_of_",$split[8], ";Parent=", $name,
                "\n";
        }
}

close GTF;
close OUTPUT;
##################################################################################################################################################################
##################################################################################################################################################################
