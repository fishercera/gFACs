![gFACs](docs/gFACs_logo.jpg)


### gFACs is a filtering, analysis, and conversion tool to unify genome annotations across alignment and gene prediction frameworks. It was developed by Madison Caballero and Dr. Jill Wegrzyn of the Plant Computational Genomics Lab at the University of Connecticut.
Version 1.1.2 - 07/17/2020

How to cite:

[Caballero M. and Wegrzyn J. 2019. gFACs: Gene Filtering, Analysis, and Conversion to Unify Genome Annotations Across Alignment and Gene Prediction Frameworks.
    Genomics, Proteomics & Bioinformatics 17: 305-10](https://doi.org/10.1016/j.gpb.2019.04.002)

## Information
Full documentation on [readthedocs](https://gfacs.readthedocs.io/en/latest/index.html).

**New with 1.1.2**
- New Braker formats!
- All fasta output will be Start --> Stop (or M--> Stop) oriented regardless of strand.
- Ability to sort by chromosome.
- StringTie compatibility.
- Checks an inputted genome for scaffold matching to avoid bioperl errors.

![gFACs](docs/flow_chart_gFACs.jpg)

Contact: Madison.Caballero@uconn.edu

---------

Copyright (C) 2018 Madison Caballero

gFACs is protected under the GNU General Public License Version 3.
