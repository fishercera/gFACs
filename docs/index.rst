.. gFACs documentation master file, created by
   sphinx-quickstart on Fri Jun 23 12:05:06 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. image:: gFACs_logo.jpg
    :align: center
    
    

Introduction to gFACs
===================
**gFACs is a filtering, analysis, and conversion tool to unify genome annotations across alignment and gene prediction frameworks. It was developed by Madison Caballero and Dr. Jill Wegrzyn of the Plant Computational Genomics Lab at the University of Connecticut.**

Find gFACs on `GitLab <https://gitlab.com/PlantGenomicsLab/gFACs>`_. 

Version 1.1.2 - 07/17/2020

How to cite:

`Caballero M. and Wegrzyn J. 2019. gFACs: Gene Filtering, Analysis, and Conversion to Unify Genome Annotations Across Alignment and Gene Prediction Frameworks. Genomics_ Proteomics & Bioinformatics 17: 305-10 <https://doi.org/10.1016/j.gpb.2019.04.002>`_ 

Comments? Questions? Email me at Madison.Caballero@uconn.edu

.. image:: flow_chart_gFACs.jpg
    :align: center

.. toctree::
   :maxdepth: 1
   :caption: Getting started
   :name: sec-general

   Getting_Started/installation.rst
   Getting_Started/Formats.rst
   Getting_Started/About.rst
   
.. toctree::
    :maxdepth: 3
    :caption: Running gFACs
    :name: sec-run
    
    Flags/index.rst
    Flags/Output_flags.rst

.. toctree::
    :maxdepth: 2
    :caption: Support scripts
    :name: sec-run
    
    Bonus/format_diagnosis.rst



 

.. Indices and tables
.. ------------------
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
